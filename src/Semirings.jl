# SPDX-License-Identifier: CECILL-2.1

module Semirings

using ChainRulesCore
import LogExpFunctions: logaddexp

export val

# Types.
export Semiring,
       ArcticSemiring,
       BoolSemiring,
       LogSemiring,
       LogExpectationSemiring,
       ProbSemiring,
       ProbExpectationSemiring,
       TropicalSemiring

# Partial derivative functions.
export ∂sum, ∂rmul, ∂lmul


###############################################################################
# Abstract semiring type.

"""
    abstract type Semiring end

Abstract type for a semiring ``(S, \\oplus, \\otimes, \\bar{0}, \\bar{1})``.
"""
abstract type Semiring{T} end


"""
    val(x::Semiring)

Return the "real" value / object wrapped in the semiring type
"""
val(x::Semiring) = x.val


"""
    Base.valtype(::Type{<:Semiring{T}}) where T

Return the type of the value wrapped by the semiring.
"""
Base.valtype(::Type{<:Semiring{T}}) where T = T


Base.zero(x::Semiring) = zero(typeof(x))
Base.one(x::Semiring) = one(typeof(x))
Base.inv(x::Semiring) = inv(typeof(x))


function Base.:*(i::Integer, s::Semiring)
    i < 0 && throw(ArgumentError("integer has to be positive or zero"))
    iszero(i) && return zero(s)

    res = zero(s)
    for n in 1:i
        res = res + s
    end
    res
end
Base.:*(s::Semiring, i::Integer) = i * s


Base.convert(T::Type{<:Semiring}, x::Number) = T(x)
Base.convert(T::Type{<:Semiring}, x::Semiring) = T(val(x))

Base.:(==)(x::Semiring, y::Semiring) = val(x) == val(y)
Base.isequal(x::Semiring, y::Semiring) = isequal(val(x), val(y))
Base.isapprox(x::Semiring, y::Semiring) = val(x) ≈ val(y)

Base.show(io::IO, x::Semiring) = print(io, val(x))


###############################################################################
# Differentiation API.

"""
    ∂sum(z, x)

Compute the partial derivative of `z = x ⊕ y ⊕ z ⊕ ...` w.r.t. `x`.
"""
∂sum
"""
    ∂rmul(x, a)

Compute the partial derivative of `x ⊗ a` w.r.t. `x`.
"""
∂rmul

"""
    ∂lmul(a, x)

Compute the partial derivative of `a ⊗ x` w.r.t. `x`.
"""
∂lmul


function ChainRulesCore.rrule(::typeof(val), a::Semiring)
    b = val(a)
    function val_pullback(b̄)
        ZeroTangent(), Tangent{typeof(a)}(; val=b̄)
    end
    b, val_pullback
end


function ChainRulesCore.rrule(S::Type{<:Semiring}, a)
    b = S(a)
    function semiring_pullback(b̄)
        ZeroTangent(), b̄.val
    end
    b, semiring_pullback
end


###############################################################################
# Boolean semiring.

"""
    struct BoolSemiring <: Semiring{Bool}
        val::Bool
    end

Boolean semiring: ``\\langle \\{0, 1\\}, \\lor, \\land, 0, 1 \\rangle``.
"""
struct BoolSemiring <: Semiring{Bool}
    val::Bool
end

Base.:+(x::BoolSemiring, y::BoolSemiring) = BoolSemiring(x.val || y.val)
Base.:*(x::BoolSemiring, y::BoolSemiring) = BoolSemiring(x.val && y.val)
Base.zero(::Type{<:BoolSemiring}) = BoolSemiring(false)
Base.one(::Type{<:BoolSemiring}) = BoolSemiring(true)
Base.inv(x::BoolSemiring) = BoolSemiring(true)

Base.convert(T::Type{BoolSemiring}, x::Semiring) = T(! iszero(x))

###############################################################################
# Probability Semiring (i.e. natural semiring).

"""
    struct ProbSemiring{T<:AbstractFloat} <: Semiring{T}
        val::T
    end

Probability semiring ``( (\\mathbb{R}_+``, +, \\cdot, 0, 1 )``.
"""
struct ProbSemiring{T<:AbstractFloat} <: Semiring{T}
    val::T
end

Base.:+(x::ProbSemiring, y::ProbSemiring) = ProbSemiring(val(x) + val(y))
Base.:*(x::ProbSemiring, y::ProbSemiring) = ProbSemiring(val(x) * val(y))
Base.zero(S::Type{<:ProbSemiring{T}}) where T = S(zero(T))
Base.one(S::Type{<:ProbSemiring{T}}) where T = S(one(T))
Base.inv(x::S) where S<:ProbSemiring = S(inv(val(x)))

∂sum(z::S, x::S) where S<:ProbSemiring  = one(valtype(S))
∂rmul(x::S, a::S) where S<:ProbSemiring = val(a)
∂lmul(a::S, x::S) where S<:ProbSemiring = val(a)


###############################################################################
# Log semiring.

"""
    struct LogSemiring{T,b} <: Semiring{T}
        val::T
    end

Logarithmic semiring: ``(\\mathbb{R} \\cup \\{- \\infty \\}, \\oplus_{\\log}, +, -\\infty, 0)``
where

```math
x \\oplus y = \\frac{1}{\\tau} \\log ( e^{\tau x} + e^{\tau y} ).
```
"""
struct LogSemiring{T<:AbstractFloat,b} <: Semiring{T}
    val::T
end

# Logaddexp with for arbitray base `b`.
_logaddexp(b, x, y) = inv(b) * logaddexp(b*x, b*y)

function ChainRulesCore.rrule(::typeof(_logaddexp), b, x, y)
    z = _logaddexp(b, x, y)

    function _logaddexp_pullback(z̄)
        if x == y == -Inf
            (NoTangent(), NoTangent(), 0, 0)
        else
            (NoTangent(), NoTangent(), exp(b*(x - z)) * z̄, exp(b*(y - z)) * z̄)
        end
    end

    z, _logaddexp_pullback
end


Base.:+(x::LogSemiring{T,b}, y::LogSemiring{T,b}) where {T,b} = LogSemiring{T,b}(_logaddexp(b, val(x), val(y)))
Base.:*(x::S, y::S) where S<:LogSemiring = S(val(x) + val(y))
Base.zero(S::Type{<:LogSemiring{T,b}}) where {T,b} = S(ifelse(b > 0, T(-Inf), T(Inf)))
Base.one(S::Type{<:LogSemiring}) = S(0)
Base.inv(x::S) where S<:LogSemiring = iszero(x) ? S(NaN) : S(-val(x))

# TODO: consider to use @thunk on this one
∂sum(z::LogSemiring{T,b}, x::LogSemiring{T,b}) where {T,b} =
    val(z) == -Inf ? zero(T) : exp(b*(val(x) - val(z)))
∂rmul(x::S, a::S) where S<:LogSemiring = valtype(S)(1)
∂lmul(a::S, x::S) where S<:LogSemiring = valtype(S)(1)


###############################################################################
# Tropical/Arctic semiring.


"""
    const TropicalSemiring{T} = LogSemiring{T,-Inf} where T

Tropical semiring: ``(\\mathbb{R} \\cup \\{- \\infty \\}, min, +, \\infty, 0)``.
"""
const TropicalSemiring{T} = LogSemiring{T,-Inf} where T
Base.:+(x::S, y::S) where S<:TropicalSemiring = S(min(val(x), val(y)))

# WRONG
∂sum(z::S, x::S) where S<:TropicalSemiring = valtype(S)(x == z)


"""
    const ArcticSemiring{T} = LogSemiring{T,Inf} where T

Tropical semiring: ``\\langle \\mathbb{R} \\cup \\{\\infty \\}, max, +, -\\infty, 0 \\rangle``.
"""
const ArcticSemiring{T} = LogSemiring{T,Inf} where T
Base.:+(x::S, y::S) where S<:ArcticSemiring = S(max(val(x), val(y)))

# WRONG
∂sum(z::S, x::S) where S<:ArcticSemiring = valtype(S)(x == z)


###############################################################################
# Expectation semiring.

"""
    struct ExpectationSemiring{T<:AbstractFloat} <: Semiring{T}
	    prob::T
		value::T
	end

Expectation semiring: ``( ((\\mathbb{R}_+, \\mathbb{R})``, +, \\cdot, (0, 0), (1, 0) )``.
"""
struct ExpectationSemiring{P<:Semiring,V<:Semiring,F} <:Semiring{Tuple{P,V}}
    prob::P
    value::V
end

val(x::ExpectationSemiring) = x.prob, x.value
Base.:+(x::ExpectationSemiring{P,V,F}, y::ExpectationSemiring{P,V,F}) where {P,V,F} =
    ExpectationSemiring{P,V,F}(x.prob + y.prob, x.value + y.value)
Base.:*(x::ExpectationSemiring{P,V,F}, y::ExpectationSemiring{P,V,F}) where {P,V,F} =
    ExpectationSemiring{P,V,F}(
        x.prob * y.prob,
        (F(x.prob) * y.value) + (x.value * F(y.prob))
    )

Base.zero(T::Type{<:ExpectationSemiring{P,V}}) where {P,V}= T(zero(P), zero(V))
Base.one(T::Type{<:ExpectationSemiring{P,V}}) where {P,V} = T(one(P), one(V))
# Base.inv(T::Type{<:ExpectationSemiring{P,V}}) where {P,V} = T()


const ProbExpectationSemiring{T} = ExpectationSemiring{ProbSemiring{T},ProbSemiring{T},identity} where {T}
const LogExpectationSemiring{T} = ExpectationSemiring{LogSemiring{T, 1},ProbSemiring{T},exp} where {T}


###############################################################################
# Morphisms.

Base.exp(x::LogSemiring{T,1}) where T = ProbSemiring{T}(exp(val(x)))
Base.log(x::ProbSemiring{T}) where T = LogSemiring{T,1}(log(val(x)))

end
