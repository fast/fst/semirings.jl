# Semirings.jl

A julia package for [semirings](https://en.wikipedia.org/wiki/Semiring).

This package is largely inspired from [JuliaComputing/SemiringAlgebra](https://github.com/JuliaComputing/SemiringAlgebra.jl)
and [mcognetta/Semirings.jl](https://github.com/mcognetta/Semirings.jl).

## Installation

This package is part of [FAST](https://gitlab.lisn.upsaclay.fr/fast).
Add the [FAST registry](https://gitlab.lisn.upsaclay.fr/fast/registry)
to your julia installation:
```
pkg> add registry "https://gitlab.lisn.upsaclay.fr/fast/registry"
```
then, install the package:

```julia
pkg> add Semirings
```

## Example

Here is a brief example:
```julia
julia> using Semirings

julia> a, b, c = S(1), S(2), S(3)
(1.0, 2.0, 3.0)

julia> a ⊗ (b ⊕ c)
4.313262

julia> zero(S)
-Inf

julia> one(S)
0.0
```

## Author

* [Lucas ONDEL YANG (LISN, CNRS)](mailto:lucas.ondel@cnrs.fr)
* Martin KOCOUR (BUT)
* Pablo Riera (CONICET)

## License

This software is provided under the CeCILL license (see [`LICENSE`](/LICENSE)).

