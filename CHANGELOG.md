# Releases

## [2.0.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v2.0.1) - 26.03.2024
### Fixed
- conversion between semiring values not working

## [2.0.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v2.0.0) - 29.02.2024
### Changed
- Do not used the `\oplus` and `\otimes` operator and used the regular `+` and
  `*` operators.
### Added
- comparison of semiring values directly with `==` and `isapprox`

## [1.7.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.7.0) - 29.02.2024
### Added
- Inverse function for divisible semiring

## [1.6.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.6.0) - 22.02.2024
### Added
- Expectation semiring with two specific instantiation, one using the
  probability  semiring the other one the log semiring

## [1.5.7](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.5.7) - 08.01.2024
### Fixed
- type `BoolSemiring` has a redundant type parameter `T:<Bool`

## [1.5.6](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.5.6) - 06.01.2024
### Fixed
- stability issue for the derivative of logaddexp

## [1.5.5](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.5.5) - 05.01.2024
### Fixed
- logaddexp senisitivity of the first argument

## [1.5.4](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.5.4) - 05.01.2024
### Fixed
- logaddexp senisitivity typo

## [1.5.3](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.5.3) - 05.01.2024
### Fixed
- logaddexp senisitivity in rrule is a Number

## [1.5.2](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.5.2) - 05.01.2024
### Fixed
- correct derivative of the logaddexp function by multiplying with senisitivity

## [1.5.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.5.1) - 05.01.2024
### Fixed
- correct derivative of the logaddexp function

## [1.5.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.5.0) - 05.01.2024
### Changed
- generic differentiation API (`∂sum`, `∂rmul`, and `∂lmul`) to write
### Improved
- Reorganized code-source to have only source file

## [1.4.3](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.4.3) - 21.12.2023
### Fixed
- Derivative of the log semiring for the oplus sum returns a tangent type

## [1.4.2](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.4.2) - 21.12.2023
### Fixed
- Derivative of the log semiring for the oplus sum of zero arguments

## [1.4.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.4.1) - 21.12.2023
### Fixed
- Missing gradient function for ProbSemiring

## [1.4.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.4.0) - 18.12.2023
### Added
- differentiaion rules for `val` and `semiring` constructor
- generic differentiation API (`∂add`, `∂times∂x`, and `∂times∂y`) to write
  efficient differentiation rules

## [1.3.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.3.0) - 15.11.2023
### Added
- Left and right integer multiplication of semirings

## [1.2.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.2.1) - 06.08.2023
### Fixed
- Converting number to scalar semiring

## [1.2.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.2.0) - 06.08.2023
### Changed
- do not consider usual type (Float64, Int, ...) as semirings
### Added
- log semiring as a scale parameters.
- arctic and the tropical semirings are special cases of the log semiring
- product semiring

## [1.1.3](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.1.3) - 15.06.2023
### Fixed
- stack overflow when using semiring/numbers

## [1.1.2](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.1.2) - 19.05.2023
### Fixed
- supports classical operators (`+`, `*`, `/`) for backward
  compatibility.

## [1.1.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.1.1) - 23.04.2023
### Fixed
- Numeric semirings inherit from the type `Number` allowing used of
  some of basic LinearAlgebra functions with arrays of semirings.

- ⊗, ⊕, and ⊘ defined for natural numbers
## [1.1.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.1.0) - 23.04.2023
### Added
- ⊗, ⊕, and ⊘ defined for natural numbers

## [1.0.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v1.0.0) - 23.04.2023
### Changed
- The API is now based on the ⊗, ⊕ (and ⊘) operators. Natural addition
  and multiplication operators are not supported anymore.
- Many functions has been removed as they were only patches to
  particular behavior in downstream package (i.e. SparseArrays),
## Added
- derivatives with the LogSemiring, ProbSemiring and TropicalSemiring
  are full supported.

## [0.10.3](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.10.3) - 19.04.2023
### Fixed
- multiplication with boolean returns the correct "zero"/ "one"

## [0.10.2](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.10.2) - 19.04.2023
### Fixed
- \otimes, \oplus and \oslash operator working with natural numbers

## [0.10.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.10.1) - 18.04.2023
### Fixed
- gradient for divisible semirings

## [0.10.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.10.0) - 18.04.2023
### Added
- operators for semiring addition, multiplication (and division) are
  \oplus, \otimes (and \oslash)
- automatic differentiation for computation with log and prob semiring

## [0.9.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.9.0) - 17.04.2023
### Added
- division operator for probability and log semiring.

## [0.8.2](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.8.2) - 22.01.2023
### Fixed
- `Base.promote_op` operation returns too wide type

## [0.8.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.8.1) - 20.01.2023
### Fixed
- fixed printing of ProductSemiring

## [0.8.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.8.0) - 19.01.2023
### Added
- added `conj` function (removed in v0.7.0) for scalar and product semirings.
### Changed
- `ProductSemiring` is now a real type wrapping a tuple rather than
  an alias over a tuple type.

## [0.7.2](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.7.2) - 19.01.2023
### Fixed
- handling of boolean - product semiring multplication
- no overriding of approximation operator for non-numeric semiring

## [0.7.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.7.1) - 17.01.2023
### Fixed
- removed double-quote when printing StringMonoid value.

## [0.7.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.7.0) - 17.01.2023
### Added
- added integer multiplication
### Changed
- improved pretty printing of semiring values.
- Splitted `Semiring` type into `ScalarSemiring` and `ProductSemiring`
  types.
### Removed
- removed the complex conjucate function `conj`
- removed the absolute value function `abs`

## [0.6.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.6.0) - 13.01.2023
### Changed
- removed the division operation for divisible semiring
- divisible semring can now use the `inv(x)` function to return the
  mutiplicative inverse of the semiring

## [0.5.6](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.5.6) - 13.01.2023
### Changed
- reverted back to the `logaddexp` function from the `LogExpFunctions` package
- changed license from MIT to CeCILL 2.1

## [0.5.5](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.5.5) - 26.07.2022
### Fixed
- using fast/fster implementation of `logaddexp` function (with small loss
  of accuracy).

## [0.5.4](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.5.4) - 26.07.2022
### Fixed
- `hash` not defined for `Semiring` nor for `Monoid` leading to a search issues.

## [0.5.3](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.5.3) - 08.06.2022
### Fixed
- `convert(., .)` works in both direction from/to semiring/number for
  numeric type semirings

## [0.5.2](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.5.2) - 08.06.2022
### Fixed
- `isless(x, y)` not defined for `Ordered` semirings (e.g. LogSemiring, TropicalSemiring,...)

## [0.5.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.5.1) - 08.06.2022
### Added
- compatibility with Julia 1.6

## [0.5.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.5.0) - 01.06.2022
### Added
- append-concatenation semiring (`AppendConcat`), similar to
  union-concatenation but with list concatentation instead of union
  of set
- monoid types: `StringMonoid` and `SequenceMonoid` that are used to
  specialize `UnionConcat` and `AppendConcat` semirings

## [0.4.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.4.0) - 20.05.2022
### Added
- added the `ProductSemiring`, i.e. the combination of two semirings
- added the `StringSemiring`
### Changed
- The Union-Concatenation semiring is now restricted to work on strings
  and not arbitrary sequences
### Fixed
- Removed the `UnionAll` in the `show` method. This type signature was
  causing printing issue with the PythonCall package.

## [0.3.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.3.0) - 04.05.2022
### Added
- `val(x)` function to access the value of the semiring number
- `abs(x)` return `x` for semiring values

### Fixed
- `K(true / false)` does not give the semiring 1 / 0
- `oneunit(x)` failing due to the impossibility to build
  a semiring number from another semiring number
- `conj(x)` returning the identity function for the Union-Concatenation
  semiring.

## [0.2.1](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.2.1) - 25.04.2022
### Fixed
- `iszero(x)` not returning the semiring zero

## [0.2.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.2.0) - 23.04.2022
### Changed
- rename the package from `SemifieldAlgebra` to `Semirings`

## [0.1.0](https://gitlab.lisn.upsaclay.fr/fast/fst/Semirings.jl/-/tags/v0.1.0) - 04.04.2021
- initial release
